from fastapi import APIRouter

router = APIRouter()

@router.get('/test1')
async def hello1():
  return {"message":"Hello 1"}

@router.get('/test2')
async def hello2():
  return {"message":"Hello 2"}

@router.get('/test3')
async def hello3():
  return {"message":"Hello 3"}
