from fastapi import APIRouter, Depends, status, Response
from sqlalchemy.orm import Session
from app.models.pydantic import schemas
from app.services.db import get_db
from app.models.db import users_models

router = APIRouter()

@router.post('/singin', status_code = status.HTTP_201_CREATED)
async def create_user(request: schemas.Users, db: Session = Depends(get_db.get_db)):
  new_user = users_models.Users(name=request.name, password=request.password)
  db.add(new_user)
  db.commit()
  db.refresh(new_user)

  return new_user

@router.get('/users', status_code = status.HTTP_200_OK)
async def all_users(db: Session = Depends(get_db.get_db)):
  users = db.query(users_models.Users).all()

  return users

@router.get('/user/{id}', status_code = status.HTTP_200_OK)
async def get_user(id: int, response: Response, db: Session = Depends(get_db.get_db)):
  user = db.query(users_models.Users).filter(users_models.Users.id == id).first()
  if not user:
    response.status_code = status.HTTP_404_NOT_FOUND
    return {"detail": f"User with id {id} is not available"}

  return user
