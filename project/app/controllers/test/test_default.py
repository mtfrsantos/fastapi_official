from fastapi.testclient import TestClient
from app.app import app

client = TestClient(app)

def test_hello1():
    response = client.get("/test1")
    assert response.status_code == 200
    assert response.json() == {"message":"Hello 1"}

def test_hello2():
    response = client.get("/test2")
    assert response.status_code == 200
    assert response.json() == {"message":"Hello 2"}
