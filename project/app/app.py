from fastapi import FastAPI
from app.controllers import default_routers, users_routers
from app.models.db import users_models
from app.db import engine

users_models.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(default_routers.router)
app.include_router(users_routers.router)
