#!/bin/bash

docker kill fastapi_container
docker rm fastapi_container
docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
docker rmi fastapi
docker network rm fastapi-net
find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
rm -rf ./project/.pytest_cache
rm -rf ./.db_data
