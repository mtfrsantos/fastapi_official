FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-slim

ENV REQUIREMENTS_PATH=/project/requirements.txt
ENV ROOT_PATH=/
ENV WORKDIR=/project
ENV HOST_START_SCRIPT=/scripts/start-fastapi.sh
ENV LOCAL_START_SCRIPT=/start.sh
ENV START_SCRIPT=/start-fastapi.sh

RUN python -m pip install --upgrade pip

COPY ${HOST_START_SCRIPT} ${ROOT_PATH}

RUN chmod +x ${START_SCRIPT}

RUN mkdir -p ${WORKDIR}
WORKDIR ${WORKDIR}

CMD ${START_SCRIPT}
